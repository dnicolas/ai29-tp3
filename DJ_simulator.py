# initialisation
import numpy as np
 
# librairies Qiskit
from qiskit import IBMQ, Aer
from qiskit.providers.ibmq import least_busy
from qiskit import QuantumCircuit, assemble, transpile
 
# importation des bibliothèques d’histogramme
from qiskit.visualization import plot_histogram
 
from qiskit import Aer
 
 
def dj_oracle(case, n):
    # Il faut créer un objet QuantumCircuit
    # ce circuit possède n+1 qubits: la taille des entrées + un qubit de sortie
    oracle_qc = QuantumCircuit(n+1)
   
    # 1er cas: “Oracle Balanced”
    if case == "balanced":
        # On génère un entier aléatoire pour déterminer le nb de portes X à utiliser
        b = np.random.randint(1,2**n)
        # On convertit b en une chaîne de caractères binaire de longueur n
        b_str = format(b, '0'+str(n)+'b')
        # On place les portes X, chaque bit dans b_str correspond à 1 qubit     
 # si le bit = 0,on ne fait rien, si le bit = 1, on applique la porte X au
        # qubit
 
        for qubit in range(len(b_str)):
            if b_str[qubit] == '1':
                oracle_qc.x(qubit)
        # On applique les portes C-NOT à chaque qubit en ciblant le qubit de sortie
 
        for qubit in range(n):
            oracle_qc.cx(qubit, n)
        # On place des portes X pour restaurer l'état d’origine des qubits utilisés
        for qubit in range(len(b_str)):
            if b_str[qubit] == '1':
                oracle_qc.x(qubit)
 
    # 2e cas: oracle constant
    if case == "constant":
        # On choisit la valeur constante de la fonction (0 ou 1)
        output = np.random.randint(2)
        if output == 1:
            oracle_qc.x(n)
   
    oracle_gate = oracle_qc.to_gate()
    oracle_gate.name = "Oracle" #Nom à afficher dans le schéma
    return oracle_gate
 
 
def dj_algorithm(oracle, n):
    #On initialise les qubits d’entrée dans l’état|+⟩
    # et le qubit de sortie dans l’état|−⟩
    dj_circuit = QuantumCircuit(n+1, n)
    # On applique une porte X puis une porte H au qubit de sortie:
    dj_circuit.x(n)
    dj_circuit.h(n)
    # On applique une portes H à chaque qubit d’entrée
    for qubit in range(n):
        dj_circuit.h(qubit)
 
    # On intègre l’oracle au circuit
    dj_circuit.append(oracle, range(n+1))
    # On applique de nouveau une portes H à chaque qubit d’entrée puis on mesure
    for qubit in range(n):
        dj_circuit.h(qubit)
       
    for i in range(n):
        dj_circuit.measure(i, i)
   
    return dj_circuit
 
n = 3  
oracle_gate = dj_oracle('balanced', n)
dj_circuit = dj_algorithm(oracle_gate, n)
dj_circuit.draw()
 
aer_sim = Aer.get_backend('aer_simulator')
 
transpiled_dj_circuit = transpile(dj_circuit, aer_sim)
qobj = assemble(transpiled_dj_circuit)
results = aer_sim.run(qobj).result()
answer = results.get_counts()
plot_histogram(answer)
