# importing Qiskit
from qiskit import IBMQ, Aer
from qiskit.providers.ibmq import least_busy
from qiskit import QuantumCircuit, transpile, assemble
from qiskit.tools.monitor import job_monitor

# import basic plot tools
from qiskit.visualization import plot_histogram
from qiskit_textbook.tools import simon_oracle
 
b = '11'
 
n = len(b)
simon_circuit = QuantumCircuit(n*2, n)
 
# Apply Hadamard gates before querying the oracle
simon_circuit.h(range(n))    
   
# Apply barrier for visual separation
simon_circuit.barrier()
 
simon_circuit += simon_oracle(b)
 
# Apply barrier for visual separation
simon_circuit.barrier()
 
# Apply Hadamard gates to the input register
simon_circuit.h(range(n))
 
# Measure qubits
simon_circuit.measure(range(n), range(n))
simon_circuit.draw()
 
# use local simulator
shots = 4096

print("begin")
IBMQ.load_account()
provider = IBMQ.get_provider(hub='ibm-q')
backend = least_busy(provider.backends(filters=lambda x: x.configuration().n_qubits >= (n+1) and not x.configuration().simulator and x.status().operational==True))
print("least busy backend: ", backend)

transpiled_simon_circuit = transpile(simon_circuit, backend, optimization_level=3)
qobj = assemble(transpiled_simon_circuit, shots=shots)
job = backend.run(qobj)
job_monitor(job, interval=2)

device_counts = job.result().get_counts()
plot_histogram(device_counts)
# Calculate the dot product of the results
def bdotz(b, z):
    accum = 0
    for i in range(len(b)):
        accum += int(b[i]) * int(z[i])
    return (accum % 2)
 
for z in device_counts:
    print( '{}.{} = {} (mod 2)'.format(b, z, bdotz(b,z)) )
