# Algorithmes quantiques
## Description
Implémentations des algorithmes de Deutsch-Jozsa et de Simon en Python avec la bibliothèque Qiskit. L'exécution de ces algorithmes a été réalisée sur le simulateur d'IBM Quantum.

## Fichiers
* **DJ_IBM.py** : code source python de l'algorithme de Deutsch-Jozsa pour une exécution sur machine réelle en sollicitant les serveurs d'IBM.
*  **DJ_simulator.py** : code source python de l'algorithme de Deutsch-Jozsa pour une exécution sur simulateur.
* **Simon_IBM.py** : code source python de l'algorithme de Simon pour une exécution sur machine réelle en sollicitant les serveurs d'IBM.
*  **Simon_simulator.py** : code source python de l'algorithme de Simon pour une exécution sur simulateur.
* **Rapport_TP3.pdf** : compte rendu sur le TP3.

## Contributeurs
Yassine Ouraq - Dimitri Nicolas - AI29
